# CI Rules - De Morgan's Law POC

## Overview

GitLab-CI job rules only allow `OR` type logic when presenting multiple conditions.  That is, the `rules` are excecuted in order, the first one to test truthy wins, and any remaining rules are ignored.  

For example:

```
# Goal:  All conditions must test true, otherwise the job is not created.
# The following pseudo logic is NOT possible using standard tactics in GitLab
# In a real-life situation, each of these conditions might contain multiple complex tests.

  rules:
    - if: "A" == "A"      # Implied 'AND', but exits early doing nothing.
    - if: "B" == "B"      # Never tested
      when: always
    - when: never


# Alternatively...

  rules:
    - if: "A" == "A"      
      when: always         # Job runs here
    - if: "B" == "B"       # Never tested
      when: always
    - when: never
``` 

This creates challenges when trying to combine and test a variety of complex conditions.  This tends to result in us writing many similar, but slightly different sets of rules - and future maintenance headaches.

The goal of this repo is to demonstrate a rule-set leveraging [De Morgan's Law](https://en.wikipedia.org/wiki/De_Morgan's_laws) to create boolean `AND` type logic for GitLab-CI `rules`.  The premise of this concept as it applies to GitLab is that the job is always executed when certain double-negative conditions are met, resulting in _pseudo-AND_ logic.

_Note:_ This is different than simply using boolean conditions in one rule set, such as:
```
rules:
  - if: "A" == "A" &&
        "B" == "B"
``` 
Simple conditions like this are useful for testing basic logical conditions, but tend to grow unsustainably. 

Instead, the De Morgan's approach allows our `.gitlab-ci.yml` to be more _DRY_ by allowing us to merge multiple complex conditions into one set of `AND`-like rules, without having to create many permutations of all the combinations.

## Example

```
variables:
  A: "Some-A-Conditions"
  B: "Some-B-Conditions"

.if_not_A_then_never:
  - if: $A != "Some-A-Conditions"
    when: never

.if_not_B_then_never:
  - if: $B != "Some-B-Conditions"
    when: never

if_A_&&_B_then_always:
  rules:
    - !reference [.if_not_A_then_never]     # A is true, (tests false, so try next)
    - !reference [.if_not_B_then_never]     # B is true, (tests false, so try next)
    - when: always                          # A && B, therefore run the job
```
## In Action
See this logic running by viewing the `.gitlab-ci.yml` file in this repo and any [pipelines](https://gitlab.com/mountainmikenm/ci-rules-de-morgans-law-poc/-/pipelines) triggered by the `main` branch.
